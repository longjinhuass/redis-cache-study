package com.cache;

import com.cache.dao.MajarDao;
import com.cache.entity.Majar;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootTest
class RedisCacheStudyApplicationTests {

    @Autowired
    private MajarDao majarDao;

    @Test
    void contextLoads() {
        Majar majar = majarDao.selectById(1);
        System.out.println(majar);
    }

}
