package com.cache.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cache.entity.Majar;

/**
 * (Majar)表服务接口
 *
 * @author LongJinHua
 * @since 2023-02-18 18:52:30
 */
public interface MajarService extends IService<Majar> {

}

