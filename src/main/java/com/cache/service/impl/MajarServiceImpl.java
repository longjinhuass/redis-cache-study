package com.cache.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cache.dao.MajarDao;
import com.cache.entity.Majar;
import com.cache.service.MajarService;
import org.springframework.stereotype.Service;

/**
 * (Majar)表服务实现类
 *
 * @author LongJinHua
 * @since 2023-02-18 18:52:30
 */
@Service("majarService")
public class MajarServiceImpl extends ServiceImpl<MajarDao, Majar> implements MajarService {

}

