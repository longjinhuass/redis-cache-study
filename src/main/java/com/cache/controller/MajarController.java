package com.cache.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.ApiController;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cache.entity.Majar;
import com.cache.service.MajarService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (Majar)表控制层
 *
 * @author LongJinHua
 * @since 2023-02-18 18:52:28
 */
@RestController
@RequestMapping("majar")
public class MajarController extends ApiController {
    /**
     * 服务对象
     */
    @Resource
    private MajarService majarService;

    /**
     * 分页查询所有数据
     * @param page  分页对象
     * @param majar 查询实体
     * @return 所有数据
     */
    @Cacheable(cacheNames = "majorList", key = "#page.records")
    @GetMapping
    public R selectAll(Page<Majar> page, Majar majar) {
        return success(this.majarService.page(page, new QueryWrapper<>(majar)));
    }

    /**
     * 通过主键查询单条数据
     * @param id 主键
     * @return 单条数据
     */
    @Cacheable(cacheNames = "major", key = "#id")
    @GetMapping("{id}")
    public R selectOne(@PathVariable Serializable id) {
        return success(majarService.getById(id));
    }

    /**
     * 新增数据
     * @param majar 实体对象
     * @return 新增结果
     */
    @PostMapping
    public R insert(@RequestBody Majar majar) {
        return success(this.majarService.save(majar));
    }

    /**
     * 修改数据
     *
     * @param majar 实体对象
     * @return 修改结果
     */
    @PutMapping
    public R update(@RequestBody Majar majar) {
        return success(this.majarService.updateById(majar));
    }

    /**
     * 删除数据
     *
     * @param idList 主键结合
     * @return 删除结果
     */
    @DeleteMapping
    public R delete(@RequestParam("idList") List<Long> idList) {
        return success(this.majarService.removeByIds(idList));
    }
}

