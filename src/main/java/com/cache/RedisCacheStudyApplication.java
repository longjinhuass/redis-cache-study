package com.cache;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@MapperScan("com.cache.dao")
@EnableCaching //开启基于注解的缓存
public class RedisCacheStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisCacheStudyApplication.class, args);
    }

}
