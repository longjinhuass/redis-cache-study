package com.cache.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * (Majar)表实体类
 *
 * @author LongJinHua
 * @since 2023-02-18 18:52:29
 */
@SuppressWarnings("serial")
@Data
public class Majar implements Serializable {

    //专业代码
    @TableId(value = "id")
    private Integer id;
    //上一级专业
    private String pid;
    //专业
    private String name;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

