package com.cache.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cache.entity.Majar;

/**
 * (Majar)表数据库访问层
 *
 * @author LongJinHua
 * @since 2023-02-18 18:52:29
 */
public interface MajarDao extends BaseMapper<Majar> {

}

